include_directories(. ../buildboxworker)

# This sets up the gtest/gmock target variables
find_file(BuildboxGTestSetup BuildboxGTestSetup.cmake HINTS ${BuildboxCommon_DIR})
include(${BuildboxGTestSetup})

macro(add_worker_test TEST_NAME TEST_SOURCE)
    # Create a separate test executable per test source.
    add_executable(${TEST_NAME} ${TEST_SOURCE})

    # This allows us to pass an optional argument if the cwd for the test is not the default.
    set(ExtraMacroArgs ${ARGN})
    list(LENGTH ExtraMacroArgs NumExtraArgs)
    if(${NumExtraArgs} GREATER 0)
      list(GET ExtraMacroArgs 0 TEST_WORKING_DIRECTORY)
    else()
      set(TEST_WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR})
    endif()

    add_test(NAME ${TEST_NAME} COMMAND ${TEST_NAME} WORKING_DIRECTORY
             ${TEST_WORKING_DIRECTORY})

    target_link_libraries(${TEST_NAME} PUBLIC buildboxworker
                          Buildbox::buildboxcommon ${GTEST_MAIN_TARGET} ${GTEST_TARGET}
                          ${GMOCK_TARGET})
endmacro()

add_worker_test(botsessionutils_tests buildboxworker_botsessionutils.t.cpp)
add_worker_test(cmdlineparser_tests buildboxworker_cmdlineparser.t.cpp)
add_worker_test(executeactionutils_tests buildboxworker_executeactionutils.t.cpp)
add_worker_test(expiretime_tests buildboxworker_expiretime.t.cpp)
add_worker_test(subprocessguard_tests buildboxworker_subprocessguard.t.cpp)
add_worker_test(worker_tests buildboxworker_worker.t.cpp)
add_worker_test(actionutils_tests buildboxworker_actionutils.t.cpp ${CMAKE_CURRENT_SOURCE_DIR}/data)
add_worker_test(config_tests buildboxworker_config.t.cpp ${CMAKE_CURRENT_SOURCE_DIR}/data)
