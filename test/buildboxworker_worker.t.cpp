#include <gtest/gtest.h>

#include <google/devtools/remoteworkers/v1test2/bots_mock.grpc.pb.h>

#include <buildboxcommonmetrics_durationmetrictimer.h>
#include <buildboxcommonmetrics_testingutils.h>
#include <buildboxworker_metricnames.h>
#include <buildboxworker_worker.h>

using buildboxcommon::buildboxcommonmetrics::DurationMetricValue;
using buildboxworker::Worker;
using google::devtools::remoteworkers::v1test2::MockBotsStub;

using namespace testing;

class WorkerTests : public ::testing::Test {
  protected:
    buildboxworker::Worker worker;
    WorkerTests() { worker.d_stub = std::make_unique<MockBotsStub>(); }
};

class WorkerTestFixture : public Worker, public ::testing::Test {
  protected:
    WorkerTestFixture() {}
};

TEST_F(WorkerTestFixture, DefaultWaitTime)
{
    ASSERT_FALSE(this->d_session.has_expire_time());
    ASSERT_EQ(this->calculateWaitTime(), this->s_defaultWaitTime);
}

TEST_F(WorkerTestFixture, WaitTimeSaturates)
{
    const auto excessive_wait_time =
        std::chrono::duration_cast<std::chrono::seconds>(2 *
                                                         this->s_maxWaitTime);

    this->d_session.mutable_expire_time()->set_seconds(
        excessive_wait_time.count());

    ASSERT_TRUE(this->d_session.has_expire_time());
    ASSERT_EQ(this->calculateWaitTime(), this->s_defaultWaitTime);
}
