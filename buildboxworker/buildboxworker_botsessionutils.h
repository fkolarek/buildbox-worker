/*
 * Copyright 2020 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef INCLUDED_BUILDBOXWORKER_BOTSESSIONUTILS
#define INCLUDED_BUILDBOXWORKER_BOTSESSIONUTILS

#include <buildboxcommon_connectionoptions.h>
#include <buildboxcommon_protos.h>

#include <build/bazel/remote/execution/v2/remote_execution.grpc.pb.h>
#include <google/devtools/remoteworkers/v1test2/bots.grpc.pb.h>

namespace buildboxworker {

namespace proto {
using namespace google::devtools::remoteworkers::v1test2;
} // namespace proto

struct BotSessionUtils {
    // Metadata types:
    typedef std::multimap<grpc::string_ref, grpc::string_ref>
        GrpcServerMetadataMultiMap;

    typedef std::unordered_multimap<buildboxcommon::Digest,
                                    buildboxcommon::ExecuteOperationMetadata>
        ExecuteOperationMetadataEntriesMultiMap;

    /**
     * Helper that calls the UpdateBotSession grpc method.
     *
     * This retries the UpdateBotSession call on failures a set number of
     * times and exits if it still failed afterwards. The exiting can be
     * disabled by setting should_exit to false.
     *
     */
    static grpc::Status updateBotSession(
        const proto::BotStatus botStatus,
        std::shared_ptr<proto::Bots::StubInterface> stub,
        const long requestTimeout,
        const buildboxcommon::ConnectionOptions &botsServerConnection,
        proto::BotSession *session,
        ExecuteOperationMetadataEntriesMultiMap
            *executeOperationMetadataEntries = nullptr);

    static grpc::Status createBotSession(
        std::shared_ptr<proto::Bots::StubInterface> stub,
        const buildboxcommon::ConnectionOptions &botsServerConnection,
        proto::BotSession *session);

    /**
     * Given a multimap obtained by calling
     * `grpc::ClientContext.GetServerTrailingMetadata()`, searches for metadata
     * entries under the "executeoperationmetadata-bin" name and returns a list
     * with the `ExecuteOperationMetadata` messages they contain.
     */
    static ExecuteOperationMetadataEntriesMultiMap
    extractExecuteOperationMetadata(
        const GrpcServerMetadataMultiMap &metadata);
};

} // namespace buildboxworker

#endif
