/*
 * Copyright 2020 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxworker_cmdlinespec.h>

namespace buildboxworker {

using ArgumentSpec = buildboxcommon::CommandLineTypes::ArgumentSpec;
using DataType = buildboxcommon::CommandLineTypes::DataType;
using TypeInfo = buildboxcommon::CommandLineTypes::TypeInfo;
using DefaultValue = buildboxcommon::CommandLineTypes::DefaultValue;

CmdLineSpec::CmdLineSpec()
{
    d_spec.emplace_back("help", "Display usage and exit",
                        TypeInfo(DataType::COMMANDLINE_DT_BOOL));
    d_spec.emplace_back("instance",
                        "Instance name to pass to the Remote Workers API and "
                        "the CAS servers",
                        TypeInfo(DataType::COMMANDLINE_DT_STRING),
                        ArgumentSpec::O_OPTIONAL, ArgumentSpec::C_WITH_ARG,
                        DefaultValue(""));
    d_spec.emplace_back(
        "concurrent-jobs",
        "Number of jobs to run at once, defaults to running one job at a time",
        TypeInfo(DataType::COMMANDLINE_DT_INT), ArgumentSpec::O_OPTIONAL,
        ArgumentSpec::C_WITH_ARG, DefaultValue(1));
    d_spec.emplace_back("stop-after",
                        "Terminate after running the given number of jobs, "
                        "defaults to non-terminating",
                        TypeInfo(DataType::COMMANDLINE_DT_INT),
                        ArgumentSpec::O_OPTIONAL, ArgumentSpec::C_WITH_ARG,
                        DefaultValue(-1));
    d_spec.emplace_back("cas-remote",
                        "URL for the Content-Addressable Storage server that "
                        "job-related files"
                        " will be fetched from and uploaded to",
                        TypeInfo(DataType::COMMANDLINE_DT_STRING),
                        ArgumentSpec::O_REQUIRED, ArgumentSpec::C_WITH_ARG);
    d_spec.emplace_back("cas-access-token",
                        "Access token to use for CAS server requests",
                        TypeInfo(DataType::COMMANDLINE_DT_STRING),
                        ArgumentSpec::O_OPTIONAL, ArgumentSpec::C_WITH_ARG);
    d_spec.emplace_back("bots-remote", "The URL for the RWAPI server",
                        TypeInfo(DataType::COMMANDLINE_DT_STRING),
                        ArgumentSpec::O_REQUIRED, ArgumentSpec::C_WITH_ARG);
    d_spec.emplace_back("bots-access-token",
                        "Access token to use for RWAPI server requests",
                        TypeInfo(DataType::COMMANDLINE_DT_STRING),
                        ArgumentSpec::O_OPTIONAL, ArgumentSpec::C_WITH_ARG);
    d_spec.emplace_back(
        "request-timeout",
        "Sets the timeout for updateBotSession request, defaults to 1 second",
        TypeInfo(DataType::COMMANDLINE_DT_INT), ArgumentSpec::O_OPTIONAL,
        ArgumentSpec::C_WITH_ARG, DefaultValue(1));
    d_spec.emplace_back("buildbox-run", "Absolute path to runner exectuable",
                        TypeInfo(DataType::COMMANDLINE_DT_STRING),
                        ArgumentSpec::O_REQUIRED, ArgumentSpec::C_WITH_ARG);
    d_spec.emplace_back("bots-retry-limit",
                        "Number of times to retry on BOTS related grpc errors",
                        TypeInfo(DataType::COMMANDLINE_DT_STRING),
                        ArgumentSpec::O_OPTIONAL, ArgumentSpec::C_WITH_ARG,
                        DefaultValue("4"));
    d_spec.emplace_back("bots-retry-delay",
                        "How long to wait (in milliseconds) before the first "
                        "BOTS related grpc retry",
                        TypeInfo(DataType::COMMANDLINE_DT_STRING),
                        ArgumentSpec::O_OPTIONAL, ArgumentSpec::C_WITH_ARG,
                        DefaultValue("1000"));
    d_spec.emplace_back("cas-retry-limit",
                        "Number of times to retry on CAS related grpc errors",
                        TypeInfo(DataType::COMMANDLINE_DT_STRING),
                        ArgumentSpec::O_OPTIONAL, ArgumentSpec::C_WITH_ARG,
                        DefaultValue("4"));
    d_spec.emplace_back("cas-retry-delay",
                        "How long to wait (in milliseconds) before the first "
                        "CAS related grpc retry",
                        TypeInfo(DataType::COMMANDLINE_DT_STRING),
                        ArgumentSpec::O_OPTIONAL, ArgumentSpec::C_WITH_ARG,
                        DefaultValue("1000"));
    d_spec.emplace_back(
        "runner-arg",
        "Arguments to pass buildbox-run when the worker runs a job\n"
        "This can be useful if the buildbox-run implementation you're using "
        "supports non-standard options",
        TypeInfo(DataType::COMMANDLINE_DT_STRING_ARRAY),
        ArgumentSpec::O_OPTIONAL, ArgumentSpec::C_WITH_ARG);
    d_spec.emplace_back("platform",
                        "Add a key-value pair to the 'Platform' message the "
                        "worker sends to the server\n"
                        "(see the Remote Workers API specification for keys "
                        "and values you can use)"
                        "\nformat is --platform KEY=VALUE",
                        TypeInfo(DataType::COMMANDLINE_DT_STRING_PAIR_ARRAY),
                        ArgumentSpec::O_OPTIONAL, ArgumentSpec::C_WITH_ARG);
    d_spec.emplace_back(
        "metrics-mode",
        "format is --metrics-mode=MODE where 'MODE' is one of:\n"
        "udp://<hostname>:<port>\nfile:///path/to/file\nstderr",
        TypeInfo(DataType::COMMANDLINE_DT_STRING), ArgumentSpec::O_OPTIONAL,
        ArgumentSpec::C_WITH_ARG);
    d_spec.emplace_back("metrics-publish-interval",
                        "Metrics publishing interval",
                        TypeInfo(DataType::COMMANDLINE_DT_INT),
                        ArgumentSpec::O_OPTIONAL, ArgumentSpec::C_WITH_ARG);
    d_spec.emplace_back("log-level", "Log verbosity level",
                        TypeInfo(DataType::COMMANDLINE_DT_STRING),
                        ArgumentSpec::O_OPTIONAL, ArgumentSpec::C_WITH_ARG,
                        DefaultValue("info"));
    d_spec.emplace_back("verbose", "Set log level to debug",
                        TypeInfo(DataType::COMMANDLINE_DT_BOOL),
                        ArgumentSpec::O_OPTIONAL);
    d_spec.emplace_back(
        "log-file", "Log file name", TypeInfo(DataType::COMMANDLINE_DT_STRING),
        ArgumentSpec::O_OPTIONAL, ArgumentSpec::C_WITH_ARG, DefaultValue(""));
    d_spec.emplace_back("config-file", "Absolute path to config file",
                        TypeInfo(DataType::COMMANDLINE_DT_STRING),
                        ArgumentSpec::O_OPTIONAL, ArgumentSpec::C_WITH_ARG,
                        DefaultValue(""));
    d_spec.emplace_back("", "BOT Id", TypeInfo(&d_botId),
                        ArgumentSpec::O_OPTIONAL);
};

} // namespace buildboxworker
